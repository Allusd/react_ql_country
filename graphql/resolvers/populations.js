const Population = require('../../models/population');

module.exports = {
    populations: async (args, req) => {
        try {
          const populations = await Population.find();
          let amount = 0;
          return populations.map(population => {
            amount++;
              console.log(population, amount)
            return {...population._doc};
          });
        } catch (err) {
          throw err;
        }
    },
    populationbyName: async name => {
        const population = await Population.findOne( name);
        if(!population){
            throw new Error('Cannot find data');
        }
        return {...population._doc};
    },
    populationbyCountryCode: async code => {
        const population = await Population.findOne(code);
        if(!population){
            throw new Error('Cannot find data');
        }
        return {...population._doc};
    },
    createPopulation: async args => {
        try{
            const existingPopulation = await Population.findOne({ CountryName: args.PopulationInput.CountryName });
            if(existingPopulation){
                throw new Error("That countrys population data is already in database")
        }
            const population = new Population({
            CountryName: args.PopulationInput.CountryName,
            CountryCode: args.PopulationInput.CountryCode,
            IndicatorName: args.PopulationInput.IndicatorName,
            IndicatorCode: args.PopulationInput.IndicatorCode,
            sixty: args.PopulationInput.sixty,
            sixtyone: args.PopulationInput.sixtyone,
            sixtytwo: args.PopulationInput.sixtytwo,
            sixtythree: args.PopulationInput.sixtythree,
            sixtyfour: args.PopulationInput.sixtyfour,
            sixtyfive: args.PopulationInput.sixtyfive,
            sixtysix: args.PopulationInput.sixtysix,
            sixtyseven: args.PopulationInput.sixtyseven,
            sixtyeight: args.PopulationInput.sixtyeight,
            sixtynine: args.PopulationInput.sixtynine,
            seventy: args.PopulationInput.seventy,
            seventyone: args.PopulationInput.seventyone,
            seventytwo: args.PopulationInput.seventyone,
            seventythree: args.PopulationInput.seventythree,
            seventyfour: args.PopulationInput.seventyfour,
            seventyfive: args.PopulationInput.seventyfive,
            seventysix: args.PopulationInput.seventysix,
            seventyseven: args.PopulationInput.seventyseven,
            seventyeight: args.PopulationInput.seventyeight,
            seventynine: args.PopulationInput.seventynine,
            eighty: args.PopulationInput.eighty,
            eightyone: args.PopulationInput.eightyone,
            eightytwo: args.PopulationInput.eightytwo,
            eightythree: args.PopulationInput.eightythree,
            eightyfour: args.PopulationInput.eightyfour,
            eightyfive: args.PopulationInput.eightyfive,
            eightysix: args.PopulationInput.eightysix,
            eightyseven: args.PopulationInput.eightyseven,
            eightyeight: args.PopulationInput.eightyeight,
            eightynine: args.PopulationInput.eightynine,
            ninety: args.PopulationInput.ninety,
            ninetyyone: args.PopulationInput.ninetyyone,
            ninetytwo: args.PopulationInput.ninetytwo,
            ninetythree: args.PopulationInput.ninetythree,
            ninetyfour: args.PopulationInput.ninetyfour,
            ninetyfive: args.PopulationInput.ninetyfive,
            ninetysix: args.PopulationInput.ninetysix,
            ninetyseven: args.PopulationInput.ninetyseven,
            ninetyeight: args.PopulationInput.ninetyeight,
            ninetynine: args.PopulationInput.ninetynine,
            twoK: args.PopulationInput.twoK,
            twoKone: args.PopulationInput.twoKone,
            twoKtwo: args.PopulationInput.twoKtwo,
            twoKthree: args.PopulationInput.twoKthree,
            twoKfour: args.PopulationInput.twoKfour,
            twoKfive: args.PopulationInput.twoKfive,
            twoKsix: args.PopulationInput.twoKsix,
            twoKseven: args.PopulationInput.twoKseven,
            twoKeight: args.PopulationInput.twoKeight,
            twoKnine: args.PopulationInput.twoKnine,
            twoKten: args.PopulationInput.twoKten,
            twoKeleven: args.PopulationInput.twoKeleven,
            twoKtwlelve: args.PopulationInput.twoKtwlelve,
            twoKthirteen: args.PopulationInput.twoKthirteen,
            twoKfourteen: args.PopulationInput.twoKfourteen
            });
            const result = await population.save();
            console.log(result);
            return { ...result._doc, _id:result.id}
        }catch(err){
            throw err
        }
    }
};