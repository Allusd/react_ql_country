const Emission = require('../../models/emission');


module.exports = {
    emissions: async (args, req) => {
        try {
          const emissions = await Emission.find();
          let amount = 0;
          return emissions.map(emission => {
              amount++;
              console.log(emission, amount)
            return {...emission._doc};
          });
        } catch (err) {
          throw err;
        }
    },
    emissionbyName: async name => {
        const emission = await Emission.findOne(name);
        console.log(emission)

        return {...emission._doc};
    
    },
    emissionbyCountryCode: async CountryCountryCode => {
        const emission = await Emission.findOne(CountryCountryCode);
        if(!emission){
            throw new Error('Cannot find data');
        }
        return {...emission._doc};
    },
    createEmission: async args => {

        try{
            
            const existingEmission = await Emission.findOne({ CountryName: args.EmissionInput.CountryName })

            if(existingEmission){
                throw new Error("That countrys emission data is already in database")
        }

            const emission = new Emission({
            CountryName: args.EmissionInput.CountryName,
            CountryCode: args.EmissionInput.CountryCode,
            IndicatorName: args.EmissionInput.IndicatorName,
            IndicatorCode: args.EmissionInput.IndicatorCode,
            sixty: args.EmissionInput.sixty,
            sixtyone: args.EmissionInput.sixtyone,
            sixtytwo: args.EmissionInput.sixtytwo,
            sixtythree: args.EmissionInput.sixtythree,
            sixtyfour: args.EmissionInput.sixtyfour,
            sixtyfive: args.EmissionInput.sixtyfive,
            sixtysix: args.EmissionInput.sixtysix,
            sixtyseven: args.EmissionInput.sixtyseven,
            sixtyeight: args.EmissionInput.sixtyeight,
            sixtynine: args.EmissionInput.sixtynine,
            seventy: args.EmissionInput.seventy,
            seventyone: args.EmissionInput.seventyone,
            seventytwo: args.EmissionInput.seventyone,
            seventythree: args.EmissionInput.seventythree,
            seventyfour: args.EmissionInput.seventyfour,
            seventyfive: args.EmissionInput.seventyfive,
            seventysix: args.EmissionInput.seventysix,
            seventyseven: args.EmissionInput.seventyseven,
            seventyeight: args.EmissionInput.seventyeight,
            seventynine: args.EmissionInput.seventynine,
            eighty: args.EmissionInput.eighty,
            eightyone: args.EmissionInput.eightyone,
            eightytwo: args.EmissionInput.eightytwo,
            eightythree: args.EmissionInput.eightythree,
            eightyfour: args.EmissionInput.eightyfour,
            eightyfive: args.EmissionInput.eightyfive,
            eightysix: args.EmissionInput.eightysix,
            eightyseven: args.EmissionInput.eightyseven,
            eightyeight: args.EmissionInput.eightyeight,
            eightynine: args.EmissionInput.eightynine,
            ninety: args.EmissionInput.ninety,
            ninetyyone: args.EmissionInput.ninetyyone,
            ninetytwo: args.EmissionInput.ninetytwo,
            ninetythree: args.EmissionInput.ninetythree,
            ninetyfour: args.EmissionInput.ninetyfour,
            ninetyfive: args.EmissionInput.ninetyfive,
            ninetysix: args.EmissionInput.ninetysix,
            ninetyseven: args.EmissionInput.ninetyseven,
            ninetyeight: args.EmissionInput.ninetyeight,
            ninetynine: args.EmissionInput.ninetynine,
            twoK: args.EmissionInput.twoK,
            twoKone: args.EmissionInput.twoKone,
            twoKtwo: args.EmissionInput.twoKtwo,
            twoKthree: args.EmissionInput.twoKthree,
            twoKfour: args.EmissionInput.twoKfour,
            twoKfive: args.EmissionInput.twoKfive,
            twoKsix: args.EmissionInput.twoKsix,
            twoKseven: args.EmissionInput.twoKseven,
            twoKeight: args.EmissionInput.twoKeight,
            twoKnine: args.EmissionInput.twoKnine,
            twoKten: args.EmissionInput.twoKten,
            twoKeleven: args.EmissionInput.twoKeleven,
            twoKtwlelve: args.EmissionInput.twoKtwlelve,
            twoKthirteen: args.EmissionInput.twoKthirteen,
            twoKfourteen: args.EmissionInput.twoKfourteen
            });
            const result = await emission.save();

            return { ...result._doc, _id: result.id }
        }catch(err){
            throw err;
        }
    }
};