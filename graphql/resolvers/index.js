
const emissionResolver = require('./emissions');
const populationResolver = require('./populations');



const rootResolver = {
  ...emissionResolver,
  ...populationResolver
};

module.exports = rootResolver;