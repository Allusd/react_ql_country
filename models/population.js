const mongoose = require('mongoose');


const Schema = mongoose.Schema;


const populationSchema = new Schema({
    CountryName: {
        type: String,
        required: true
    },
    CountryCode: {
        type: String,
        required: true
    },
    IndicatorName: {
        type: String,
        required: true
    },
    IndicatorCode: {
        type: String,
        required: true
    },
    sixty: {
        type: String,
        required: true
    },
    sixtyone: {
        type: String,
        required: true
    },
    sixtytwo: {
        type: String,
        required: true
    },
    sixtythree: {
        type: String,
        required: true
    },
    sixtyfour: {
        type: String,
        required: true
    },
    sixtyfive: {
        type: String,
        required: true
    },
    sixtysix: {
        type: String,
        required: true
    },
    sixtyseven: {
        type: String,
        required: true
    },
    sixtyeight: {
        type: String,
        required: true
    },
    sixtynine: {
        type: String,
        required: true
    },
    seventy: {
        type: String,
        required: true
    },
    seventyone: {
        type: String,
        required: true
    },
    seventytwo:{
        type: String,
        required: true
    },
    seventythree: {
        type: String,
        required: true
    },
    seventyfour: {
        type: String,
        required: true
    },
    seventyfive: {
        type: String,
        required: true
    },
    seventysix: {
        type: String,
        required: true
    },
    seventyseven: {
        type: String,
        required: true
    },
    seventyeight: {
        type: String,
        required: true
    },
    seventynine: {
        type: String,
        required: true
    },
    eighty: {
        type: String,
        required: true
    },
    eightyone: {
        type: String,
        required: true
    },
    eightytwo: {
        type: String,
        required: true
    },
    eightythree: {
        type: String,
        required: true
    },
    eightyfour: {
        type: String,
        required: true
    },
    eightyfive: {
        type: String,
        required: true
    },
    eightysix: {
        type: String,
        required: true
    },
    eightyseven: {
        type: String,
        required: true
    },
    eightyeight: {
        type: String,
        required: true
    },
    eightynine: {
        type: String,
        required: true
    },
    ninety: {
        type: String,
        required: true
    },
    ninetyyone: {
        type: String,
        required: true
    },
    ninetytwo: {
        type: String,
        required: true
    },
    ninetythree: {
        type: String,
        required: true
    },
    ninetyfour: {
        type: String,
        required: true
    },
    ninetyfive: {
        type: String,
        required: true
    },
    ninetysix: {
        type: String,
        required: true
    },
    ninetyseven: {
        type: String,
        required: true
    },
    ninetyeight: {
        type: String,
        required: true
    },
    ninetynine: {
        type: String,
        required: true
    },
    twoK: {
        type: String,
        required: true
    },
    twoKone: {
        type: String,
        required: true
    },
    twoKtwo: {
        type: String,
        required: true
    },
    twoKthree: {
        type: String,
        required: true
    },
    twoKfour: {
        type: String,
        required: true
    },
    twoKfive: {
        type: String,
        required: true
    },
    twoKsix: {
        type: String,
        required: true
    },
    twoKseven: {
        type: String,
        required: true
    },
    twoKeight: {
        type: String,
        required: true
    },
    twoKnine: {
        type: String,
        required: true
    },
    twoKten: {
        type: String,
        required: true
    },
    twoKeleven: {
        type: String,
        required: true
    },
    twoKtwlelve: {
        type: String,
        required: true
    },
    twoKthirteen: {
        type: String,
        required: true
    },
    twoKfourteen: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Population', populationSchema);