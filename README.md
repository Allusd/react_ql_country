# Project Title

Country Emission/Population API

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

dependencies: 

    "body-parser": "^1.18.3",

    "express": "^4.16.4",

    "express-graphql": "^0.7.1",

    "graphql": "^14.1.1",

    "mongoose": "^5.4.14"

  

  devDependencies: 

    "nodemon": "^1.18.10"

  

npm install --save body-parser

npm install --save express

npm install --save body-parser

npm install --save express-graphql

npm install --save graphql

npm install --save mongoose

npm install --save-dev nodemon

## Querys and examples

https://localhost:8000/graphql

Example querys:

query{
emissionbyCountryCode(CountryCode: $CountryCode){
      _id
      CountryName
      CountryCode
      IndicatorName
      IndicatorCode
      sixty
      sixtyone
      sixtytwo
      .
      .
      .
      .

    
    query{
    populationbyCountryCode(CountryCode: $CountryCode){
      _id
      CountryName
      CountryCode
      IndicatorName
      IndicatorCode
      sixty
      sixtyone
      sixtytwo
      sixtythree
      .
      .
      .
      .
      .

    
  

  query{
    emissions{
      _id
      CountryName
      CountryCode
      IndicatorName
      IndicatorCode
    
  

  query{
    populations{
      _id
      CountryName
      CountryCode
      IndicatorName
      IndicatorCode
    
  


## Built With

Javascript 

NodeJs Express Graphql MongoDB

React

## Authors


Aleksi Leppimaa
3rd year Student
Metropolia school of applied sciences - Tieto- ja Viestintätekniikka 
Software Progaramming/ Ohjelmistotuotanto



## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
